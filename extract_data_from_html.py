from bs4 import BeautifulSoup
import arrow
import pickle
import os

DIR = 'results_JSAI2015'

soup = BeautifulSoup(file('data_hakodate_final.html', 'r').read())

div_database = soup.find_all('div', attrs={'id':'database'})[0]
div_parameter = soup.find_all('div', attrs={'id': 'parameters'})[0]

trials = []
header = [td.text for td in div_database.find('thead').find_all('th')]

for tr in div_database.find_all('tr'):
	item = {}
	for t, td in enumerate(tr.find_all('td')):
		elem = td.text
		if header[t] == 'added_timestamp' or header[t] == 'response_timestamp':
			if elem == 'None':
				elem = None
			else:
				elem = arrow.get(elem).to('JST').datetime
		elif header[t] == 'displayed_item_pair' or header[t] == 'displayed_ranking':
			elem = eval(elem)

		item[header[t]] = elem
	if not item == {}:
		trials.append(item)

header = [td.text for td in div_parameter.find('thead').find_all('th')]
parameters = []
for tr in div_parameter.find_all('tr'):
	item = {}
	for t, td in enumerate(tr.find_all('td')):
		elem = td.text
		if header[t] == 'current_mu' or header[t] == 'current_sigma':
			elem = float(elem)
		item[header[t]] = elem
	if not item == {}:
		parameters.append(item)

pickle.dump(trials, file(os.path.join(DIR, 'trials.pickle'), 'w'))
pickle.dump(parameters, file(os.path.join(DIR, 'parameters.pickle'), 'w'))	