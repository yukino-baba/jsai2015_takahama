# README #

JSAI2015, インタラクティブセッションデモ用ソースコード

## 概要 ##
- Python3で動作
- データの受け渡し用のpickleファイルを読み込んでランキングの更新と次に比較するペアの決定を行い，それらをpickleに書き込む

## ファイル ##
- update_pickle.py: メイン
- sample.pickle: 例として作成したデータのpickleファイル．形式は下記参照
- その他: モジュール等

## 実行 ##
- pickleファイルを読み込まずに実行(最初だけ): `python update_pickle.py --start`
- pickleファイルを読み込んで実行: `python update_pickle.py`

## ファイル形式 ##
- update_pickle.pyで生成されるpickleファイルに格納されているデータ
    - トライアルごとの様々なデータを順に格納したリスト
- 暫定的に読み込み・書き出しファイル名を`test.pickle`に固定
- コーディング用に作ったサンプルは`sample.pickle`

```python3
[
    {
        'trial_id': 1, # 0からスタート
        'displayed_items': (item_id, item_id), # たとえば: (2, 3)
        'response_timestamp': '', # python側では値を入れない．HTML側で'YYYY-MM-DD HH:MM:SS'のようにする
        'response_item': '', # python側では値を入れない．HTML側で勝ったitem_idを入れる．たとえば: 3
        'estimated_parameters': {
            item_id: {'mu': XX, 'sigma': XX},
            item_id: {'mu': XX, 'sigma': XX} # キーとなるitem_idは文字列ではなく数値
        },
        'estimated_ranking': [item_id, item_id, ...] # たとえば: [3, 4, 2, 1]
    },
    {
    ...
    }
]
```