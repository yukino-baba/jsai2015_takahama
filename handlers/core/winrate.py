import math
from tblCdfMaker import tblCdf
# t = tblCdf()
from util import normcdf

class winrate:
    def __init__(self):
        return None

    def normcdf(self, x):
        # alternative way to calcurate norm.cdf without using scipy
        return (1.0 + math.erf(x / math.sqrt(2.0))) / 2.0

    def winrate(self, mu_i, mu_j, sqSigma_i, sqSigma_j):
        val = (mu_i - mu_j) / math.sqrt(sqSigma_i + sqSigma_j)
        # return t.cdf(val)
        return normcdf(val)


if __name__ == '__main__':
    wr = winrate()
    mu_i = 10
    mu_j = 90
    sqSigma_i = 147 ** 2
    sqSigma_j = 147 ** 2
    print('Probability i wins:', wr.winrate(mu_i, mu_j, sqSigma_i, sqSigma_j))
    print('Probability j wins:', wr.winrate(mu_j, mu_i, sqSigma_j, sqSigma_i))
