import numpy as np
import math

def normcdf(x):
    # alternative way to calcurate norm.cdf without using scipy
    return (1.0 + math.erf(x / math.sqrt(2.0))) / 2.0

def rankdata(b, method='average'):
    b = np.array(b)
    order = np.argsort(b)
    n = b.size
    ranks = np.empty((n,))
    dupcount = 0
    for i in xrange(n):
        inext = i + 1
        if i == n - 1 or b[order[i]] != b[order[inext]]:
            if method == 'min':
                tie_rank = inext - dupcount
            else:
                tie_rank = inext - 0.5 * dupcount
            for j in xrange(i - dupcount, inext):
                ranks[order[j]] = tie_rank
            dupcount = 0
        else:
            dupcount += 1
    return ranks