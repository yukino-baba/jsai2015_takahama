import math


class KL:

    def __init__(self):
        return None

    def KLdivergence(self, mu_old, mu_new, sqSigma_old, sqSigma_new):
        self.sigma_old = math.sqrt(sqSigma_old)
        self.sigma_new = math.sqrt(sqSigma_new)
        self.fstTerm = math.log(self.sigma_new / self.sigma_old)
        self.sndTerm = (sqSigma_old + (mu_old - mu_new) ** 2) / (2 * sqSigma_new)
        # self.fstTerm = math.log(self.sigma_old / self.sigma_new)
        # self.sndTerm = (sqSigma_new + (mu_new - mu_old) ** 2) / (2 * sqSigma_old)
        self.trdTerm = -0.5
        return self.fstTerm + self.sndTerm + self.trdTerm

    def KLDivergenceBernoulli(self, p_1, p_2):
        if p_1 == 0:
            if p_2 == 0:
                return 0
            elif p_2 == 1:
                return float('inf')
            else:
                return (1 - p_1) * math.log((1 - p_1) / (1 - p_2))
        elif p_1 == 1:
            if p_2 == 0:
                return float('inf')
            elif p_2 == 1:
                return 0
            else:
                return p_1 * math.log(p_1 / p_2)
        else:
            if p_2 == 0:
                return float('inf')
            elif p_2 == 1:
                return float('inf')
            else:
                return p_1 * math.log(p_1 / p_2) + (1 - p_1) * math.log((1 - p_1) / (1 - p_2))

        # return p_1 * math.log(p_1 / p_2) + (1 - p_1) * math.log((1 - p_1) / (1 - p_2))

if __name__ == '__main__':
    kl = KL()
    print(kl.KLdivergence(50, 60, 225, 2210))
