## -*- coding: utf-8 -*-
import logging

log = logging.getLogger(__name__)

from base import BaseHandler
from google.appengine.ext.webapp import blobstore_handlers
from google.appengine.ext import blobstore
from core import *

import sys, os
import random
import time

from models import *
from data import *

import numpy as np
N_PREVIOUS_RANKING = 5

INIT_MU=1500
INIT_SIGMA=147 ** 2

class DefaultHandler(BaseHandler):
    def init_info(self):
        ranking = IMAGES.keys()
        random.shuffle(ranking)        
        pair = random.sample(ranking, 2)

        current_parameters = {}
        for item in ranking:
            current_parameters[item] = {'mu': INIT_MU, 'sigma': INIT_SIGMA}

        trial_id = store_new_trial(pair, ranking, current_parameters)
        return ranking, pair, trial_id, current_parameters

    def estimate(self, items, estParams, sampling_type):
        lstMu = [estParams[x]['mu'] for x in items]
        if sampling_type == 'active':
            before_next = None
            lstSqSigma = [estParams[x]['sigma'] for x in items]
            comparisonStyle = 'Pairwise'
            samplingStyle = 'Active'
            utilStyle = 'CWR_OSL_KLD'
            numObj = len(items)
            s = sampling(numObj, lstMu, lstSqSigma, comparisonStyle, samplingStyle, utilStyle)
            i, j, before_next = s.sampling(numObj, lstMu, lstSqSigma, comparisonStyle, samplingStyle, before=before_next)
            pair = [items[i], items[j]]
        else:
            pair = random.sample(items, 2)

        ranking = [items[i] for i in np.argsort(rankdata(lstMu, method='min'))][::-1]
        return ranking, pair

    def update_parameters(self, trial_id, dispItems, respItem, estParams):
        g = glicko()
        i = dispItems[0]
        j = dispItems[1]
        if i == respItem:
            s_i = 1
            s_j = 0
        else:
            s_i = 0
            s_j = 1
        mu_i = estParams[i]['mu']
        mu_j = estParams[j]['mu']
        sigma_i = estParams[i]['sigma']
        sigma_j = estParams[j]['sigma']
        updatedSqSigma_i = g.updatedSquareOfSigma_i(mu_i, mu_j, sigma_i, sigma_j)
        updatedSqSigma_j = g.updatedSquareOfSigma_i(mu_j, mu_i, sigma_j, sigma_i)
        updatedMu_i = g.updatedNu_i(mu_i, mu_j, updatedSqSigma_i, sigma_j, s_i)
        updatedMu_j = g.updatedNu_i(mu_j, mu_i, updatedSqSigma_j, sigma_i, s_j)
        estParams[i]['sigma'] = updatedSqSigma_i
        estParams[j]['sigma'] = updatedSqSigma_j
        estParams[i]['mu'] = updatedMu_i
        estParams[j]['mu'] = updatedMu_j
        return estParams

    def skip(self, trial_id, ranking, current_parameters):
        trial = retrieve_trial_by_id(trial_id)
        trial.key.delete()
        pair = random.sample(ranking, 2)
        trial_id = store_new_trial(pair, ranking, current_parameters)

        return pair, trial_id

    def get(self):
        config = self.app.config
        response = self.request.get('q')
        trial_id = self.request.get('t')
        sampling_type = self.request.get('s')
        skip_flag = self.request.get('k')

        if skip_flag == '1':
            assert response == ''

        if sampling_type == '':
            sampling_type = 'random'

        # RESPONSE IS NOT GIVEN, JUST SHOW CURRENT RANKING
        if response == '':
            incomplete_trial = retrieve_latest_incomplete_trial()
            if incomplete_trial == None:
                ranking, pair, trial_id, current_parameters = self.init_info()
            else:
                ranking = incomplete_trial.displayed_ranking
                pair = incomplete_trial.displayed_item_pair
                trial_id = incomplete_trial.key.id()
                trial = retrieve_trial_by_id(trial_id)
                current_parameters = trial.current_parameters
                if skip_flag == '1':
                    assert sampling_type == 'random'
                    pair, trial_id = self.skip(trial_id, ranking, current_parameters)
                    logging.info('update')

            previous_trials = retrieve_n_complete_trials(N_PREVIOUS_RANKING)

            previous_parameters = {}
            for p_trial in previous_trials:
                p_trial_id = p_trial.key.id()
                previous_parameters[p_trial_id] = p_trial.current_parameters
                            
            self.render_response('default.html', ranking=ranking, images=IMAGES, names=NAMES, pair=pair, previous_trials=previous_trials, trial_id=trial_id, parameters=current_parameters, previous_parameters=previous_parameters, sampling_type=sampling_type)

        # RESPONSE IS GIVEN
        else:
            trial_id = long(trial_id)
            trial = retrieve_trial_by_id(trial_id)
            update = update_trial(trial_id, response)
            current_parameters = trial.current_parameters

            if update:
                estimated_parameters = self.update_parameters(trial_id, trial.displayed_item_pair, response, current_parameters)
                ranking, pair = self.estimate(IMAGES.keys(), estimated_parameters, sampling_type)                                
                trial_id = store_new_trial(pair, ranking, estimated_parameters)
                current_parameters = estimated_parameters

            else:
                incomplete_trial = retrieve_latest_incomplete_trial()
                if incomplete_trial == None:
                    ranking, pair, trial_id = self.init_info()
                else:
                    ranking = incomplete_trial.displayed_ranking
                    pair = incomplete_trial.displayed_item_pair
                    trial_id = incomplete_trial.key.id() 

            time.sleep(1)
            previous_trials = retrieve_n_complete_trials(N_PREVIOUS_RANKING)
            previous_parameters = {}
            for p_trial in previous_trials:
                p_trial_id = p_trial.key.id()
                previous_parameters[p_trial_id] = p_trial.current_parameters

            self.render_response('default.html', ranking=ranking, images=IMAGES, names=NAMES, pair=pair, previous_trials=previous_trials, trial_id=trial_id, parameters=current_parameters, previous_parameters=previous_parameters, sampling_type=sampling_type)
    
class DatabaseHandler(BaseHandler):
    def get(self):
        config = self.app.config    
        trials = retrieve_all_trials()
        parameters = {}
        for trial in trials:
            parameters[trial.key.id()] = trial.current_parameters
        self.render_response('database.html', trials=trials, parameters=parameters)

class ToggleHandler(BaseHandler):
    def get(self):
        sampling_type = self.request.get('s')
        time.sleep(2)
        if sampling_type == 'active':
            self.redirect('/?s=random')
        else:
            self.redirect('/?s=active')
		
class ResetHandler(BaseHandler):
    def get(self):
        config = self.app.config
        self.delete_all_data()
        time.sleep(2)
        self.redirect('/')

    def delete_all_data(self):
        ndb.delete_multi(TrialModel.query().fetch(keys_only=True))

    def load_items(self):
        reader = csv.reader(file(DATA_PATH, 'r'))
        for row in reader:
            header = row
            break

        for row in reader:
            item = {}
            for e, elem in enumerate(header):
                item[header[e]] = elem
            store_new_item(item['item_id'], item['item_name'], item['item_img_path'])
