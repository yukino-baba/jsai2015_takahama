## -*- coding: utf-8 -*-

NAMES = {
	'shiroikoibito': u'石屋製菓<br />白い恋人',
	'mifuyu': u'石屋製菓<br />美冬',
	'strawberry': u'六花亭<br />ストロベリーチョコ',
	'hanabatake': u'花畑牧場<br />生キャラメルプレーン味',	
	'butter': u'六花亭<br />マルセイバターサンド',
	'royce': u'ロイズ<br />生チョコレート（オーレ）',
	'san': u'柳月<br />三方六の小割',
	'raspberry': u'ボンボン製菓<br />北海道ホワイトラズベリー',
	'haskapp': u'もりもと<br />ハスカップジュエリー',
	'melon': u'ホリ<br />夕張メロンプチゴールド'
}

IMAGES = {}

for key in NAMES.keys():
	IMAGES[key] = '/static/images/png/%s.png' % key