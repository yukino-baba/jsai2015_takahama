import logging

from google.appengine.ext import ndb
from google.appengine.api import users

MAX_N_RESPONSES = 100

import datetime

class TrialModel(ndb.Model):
    displayed_item_pair = ndb.StringProperty(repeated=True)
    displayed_ranking = ndb.StringProperty(repeated=True)
    added_timestamp = ndb.DateTimeProperty(auto_now_add=True)
    response_timestamp = ndb.DateTimeProperty()
    response_item = ndb.StringProperty()
    current_parameters = ndb.PickleProperty()

def store_new_trial(displayed_item_pair, displayed_ranking, current_parameters):
    trial = TrialModel(displayed_item_pair=displayed_item_pair, displayed_ranking=displayed_ranking, current_parameters=current_parameters)
    trial.put()
    return trial.key.id()

def retrieve_all_trials():
    trials = TrialModel.query().order(-TrialModel.added_timestamp).fetch(MAX_N_RESPONSES)
    return trials

def retrieve_incomplete_trials():
    trials = TrialModel.query(TrialModel.response_item==None).order(-TrialModel.added_timestamp).fetch()
    return trials

def retrieve_n_complete_trials(n):
    trials = TrialModel.query(TrialModel.response_item!=None).fetch(MAX_N_RESPONSES)
    trials = sorted(trials, key=lambda x:x.added_timestamp, reverse=True)
    return trials[:n]

def retrieve_latest_incomplete_trial():
    trials = retrieve_incomplete_trials()
    if len(trials) == 0:
    	return None
    else:
	    return trials[0]

def retrieve_trial_by_id(trial_id):
    trial = TrialModel.get_by_id(trial_id)
    return trial

def update_trial(trial_id, response):
    trial = retrieve_trial_by_id(trial_id)
    if trial.response_item == None:
    	trial.response_timestamp = datetime.datetime.now()
    	trial.response_item = response
    	trial.put()
        return True
    else:
        return False
